#include <stdio.h>
#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#include <stdlib.h>
#include <time.h>



//Initialise le tableau (les contours + début fin)
int init();

// Créer un chemin entre le début et l'arrivée
// Le chemin est aléatoire
void dessin_chemin();


//Dessiner les murs
void dessin_murs();


//Affiche le labyrinthe dans son état actuel
void afficher();


//Création et écriture dans un fichier.txt
void ecriture_fichier(char* nom_fichier);


//lecture d'un fichier pour le traiter dans l'algorithme
void lecture_fichier(char* nom_fichier);


//  Résout le labyrinthe
//  Donne le nombre de chemins possibles
int resolution_chemin();


// Permet de retourner la valeur de HEIGHT
int return_HEIGHT();


// Permet de retourner la valeur de WIDTH
int return_WIDTH();
