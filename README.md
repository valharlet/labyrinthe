# Labyrinthe

- Quentin LEMAIRE

- Valentin HARLET

## Informations générales du projet

Ce projet a pour but de générer un labyrinthe aléatoirement et le résoudre. 
Le programme est capable d'ouvrir ou enregistrer un labyrinthe dans un fichier texte situé dans le répertoire du programme.

Quelques fichiers exemples de labyrinthes générés vous sont fournis.

## Compilation et exécution du programme

Afin de compiler le programme, il est nécessaire de :
- se rendre dans un terminal linux
- accéder au dossier contenant le programme
- exécuter la commande ``chmod +x cmd`` afin d'ajouter les autorisations nécessaires à l'exécution du fichier de commande(cmd)
- exécuter la commande `` ./cmd ``

Cette commande va permettre de faciliter la compilation en exécutant 3 commandes successives.

L'exécution du programme se fera alors avec la commande ``./prog``

Vous verrez apparaître un menu qui reviendra régulièrement au cours de l'exécution du programme. 

Ainsi vous pourrez choisir entre :
- générer un labyrinthe aléatoirement (il sera alors possible de l'enregistrer ou non)
- ouvrir un labyrinthe précédemment enregistré ou un exemple fourni
- résoudre le labyrinthe (généré ou chargé)
- quitter le programme

## Architecture du programme

Le programme est composé de 3 fichiers principaux
- main
>Fichier principal servant à orchestrer le programme
- fonction.h
>Contient les prototypes des fonctions utilisées dans le main
- fonction.c
>Contient les fonctions utilisées dans le main

## Liste des fonctions 

Voici la liste des fonctions principales étant utilisées dans ce programme:
- init()
- dessin_chemin()
- dessin_murs()
- afficher()
- ecriture_fichier(nom_fichier)
- lecture_fichier(nom_fichier)
- resolution_chemin()
