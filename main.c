/* Programme développé par Quentin LEMAIRE et Valentin HARLET dans le cadre du cours de programmation-langage C : Polytech Lille, Formation IMA, Année 2020*/

#include "fonction.h"

int main()
{
    int nbre_chemins;
    int choix;  //Variable servant au choix du menu
    int quit=0;
    int enregistre=0;
    char nom[100];

    while(quit==0)
    {
    printf("\n\t\tMENU\n\n"                                             //création et affichage du menu
           "Veuillez choisir entre ces differentes propositions:\n"
           "\t1 - Pour la configuration et generation d'un labyrinthe\n"
           "\t2 - Pour ouvrir un labyrinthe sauvegarde\n"
           "\t3 - Pour resoudre le labyrinthe actuel\n"
           "\t4 - Pour quitter\n\n");
    scanf("%i",&choix);


        switch(choix)
        {
            case 1:
                    init();             //initialisation : dimensions du labyrinthe
                    dessin_chemin();    //définition du chemin
                    dessin_murs();      //définition des murs
                    afficher();         //affichage
                    printf("\nSouhaitez vous enregistrer le labyrinthe?\n"
                           "\tOui : Tapez 1 \n"
                           "\tNon : Tapez 0\n\n");
                    scanf("%i", &enregistre);

                    if(enregistre)
                    {
                        printf("Saisissez le nom du fichier a enregistrer\n\n");
                        scanf("%s", &nom);
                        ecriture_fichier(nom);
                    }
                    break;

            case 2:
                    printf("Saisissez le nom du fichier a importer\n\n");
                    scanf("%s", &nom);
                    lecture_fichier(nom);  //ouverture du fichier et calcul des variables nécessaires
                    afficher();         //affichage
                    break;

            case 3:
                    nbre_chemins = resolution_chemin(return_HEIGHT()-2,return_WIDTH()-2);   //Résolution du labyrinthe chargé dans la console
                    printf("\nLe nombre de chemins possibles dans ce labyrinthe est de : %d\n", nbre_chemins);
                    printf("\nVoici ce que trouve l'algorithme de resolution\n\n");
                    afficher();
                    break;

            case 4:
                    quit=1;
                    break;

            default: printf("\nMerci de lire les instructions indiquees \n\n");

         }
         choix=0;

    }

    return 0;

}
