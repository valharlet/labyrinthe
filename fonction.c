#include "fonction.h"
#include "math.h"
#include <stdio.h>


int resolution;
int HEIGHT=80;
int WIDTH=80;

int i_depart = 0;
int j_depart = 0;

char labyrinthe[80][80];

int init()
{
    int i, j;
    int verif_taille;
    do
    {
        printf("Donnez les dimensions de la taille du labyrinthe souhaite\n\tLargeur : "); // Demande la taille du labyrinthe
        scanf("%d", &WIDTH);
        printf("\tHauteur : ");
        scanf("%d", &HEIGHT);

        if(WIDTH<4||HEIGHT<4)   // Permet de créer un vrai labyrinhte
        {
            verif_taille=0;
            printf("\nMerci d'augmenter la taille du tableau!\n");
        }
        else
        {
            verif_taille=1;
        }

    }while(verif_taille==0);


    for(i=0; i < HEIGHT; i++)            // Mise à zero de la matrice
    {
        for(j = 0; j < WIDTH; j++)
        {
            labyrinthe[i][j] = '#';
        }
    }
    printf("\nSouhaitez vous afficher la resolution du labyrinthe? \n\tOui : Tapez 1\n\tNon : Tapez 0\n"); //Demande d'affichage du résultat à l'utilisateur
    scanf("%d", &resolution);


}

void afficher()
{
    int i,j;

    for(i=0; i < HEIGHT; i++)   // Affichage de la matrice
    {
        for(j = 0; j < WIDTH; j++)
        {
            printf("%c", labyrinthe[i][j]);
        }
        printf("\n");
    }
}

void dessin_chemin()
{
    srand(time(NULL));  // Permet de générer un nombre aléatoire
    i_depart = 0;
    j_depart = 0;

    int current_i = 0;
    int current_j = 0;

    int depl_i;
    int depl_j;

    if(rand() % 2) // Choix de l'axe de départ
    {
        i_depart = rand() % (HEIGHT/2)+1; // Génère un nombre aléatoire entre 0 et largeur/2
    }
    else
    {
        j_depart = rand() % (WIDTH/2)+1; // Génère un nombre aléatoire entre 0 et largeur/2
    }

    current_i = i_depart;   // Initialisation du point de départ choisi aléatoirement
    current_j = j_depart;

    if(current_i==0)
    {
        current_i++;
    }
    else
    {
        current_j++;
    }
    
    if(resolution==1)
    {
        labyrinthe[current_i][current_j]='O';
    }
    else
    {
        labyrinthe[current_i][current_j]=' ';
    }

    while(!(current_i == HEIGHT-2 && current_j == WIDTH-2))
    {
        depl_i = MIN(rand()%2,HEIGHT-2-current_i);  // Déplacement pas par pas pour le chemin de résolution
        depl_j = MIN(1 - depl_i,WIDTH-2-current_j); // Les fonctions MIN permettent de limiter le déplacement jusqu'au mur

        current_i += depl_i;    // Mises à jour des positions actuelles
        current_j += depl_j;

        if(resolution==1)
        {
            labyrinthe[current_i][current_j]= 'O';  // Affichage du chemin
        }
        else
        {
            labyrinthe[current_i][current_j]=' ';
        }
    }

    labyrinthe[i_depart][j_depart]= 'D'; // Point de Départ
    labyrinthe[current_i][current_j]= 'F';  // Point d'Arrivée


}




void dessin_murs()
{
    srand(time(NULL)); //Initialisation de rand();
    int i, j;

    for(i=1; i < HEIGHT-1; i++)            // Parcours de la matrice en excluant le contour
    {
        for(j = 1; j < WIDTH-1; j++)
        {
            if(labyrinthe[i][j] == '#')   // Création des murs aléatoirement
            {
                if(rand()%2)
                {
                    labyrinthe[i][j] = ' ';
                }

            }
        }
    }
}


//Résolution du labyrinthe

int resolution_chemin(int i, int j)
{
    if(i == i_depart && j == j_depart) return 1;        // Conditions d'arrêt

    if(labyrinthe[i][j] == '#') return 0;               // Si on tombe sur un mur, on retourne 0
    if(i < i_depart) return 0;                          // Si on dépasse le point de départ, il est inutile de continuer
    if(j < j_depart) return 0;

    if(i != HEIGHT-2 || j != WIDTH-2)   // Trace les résolutions de chemins possibles
    {
        labyrinthe[i][j] = 'O';
    }


    return resolution_chemin(i, j-1)+resolution_chemin(i-1,j);
}

int return_HEIGHT() // Permet de retourner la valeur de HEIGHT
{
    return HEIGHT;
}

int return_WIDTH() // Permet de retourner la valeur DE WIDTH
{
    return WIDTH;
}


void ecriture_fichier(char* nom_fichier) // Permet d'enregistrer un labyrinthe précédemment créé
{
    int i,j;

    FILE* fichier = NULL;
    fichier = fopen(nom_fichier, "w");

    if (fichier != NULL)
    {
         for(i=0; i < HEIGHT; i++)   // Affichage de la matrice
         {
               for(j = 0; j < WIDTH; j++)
               {
                    fprintf(fichier,"%c",labyrinthe[i][j] );
               }
               fprintf(fichier,"\n");
         }
    }
    fclose(fichier);

}


void lecture_fichier(char* nom_fichier)      // Permet l'ouverture du fichier et calcul des variables nécessaires
{
    int i=0,j=0;
    int k=0, l=0;
    char caractereActuel;

    FILE* fichier = NULL;
    fichier = fopen(nom_fichier, "r");

    if (fichier != NULL)
     {
         do     //--------- Calcul des variables nécessaires (taille de la matrice et emplacement du point de départ)
         {
             caractereActuel = fgetc(fichier);
             if(caractereActuel == '\n')
             {
                l++;
             }
             else k++;

             if(caractereActuel == 'D')
             {
                 if(l > 0)
                 {
                     i_depart = l;
                     j_depart = 0;
                 }
                 else
                 {
                     j_depart = k-1;
                     i_depart = 0;
                 }
             }
        }while(caractereActuel != EOF);

         k=floor(k/l);

         WIDTH = k;
         HEIGHT = l;

    }//------------------------------------------------------------------------------------------------------------
         fclose(fichier);

         fichier = NULL;
         fichier = fopen(nom_fichier, "r");
         if (fichier != NULL)
         {
         do     //----------------------------------ouverture du labyrinthe----------------------------------------
             {
                 caractereActuel = fgetc(fichier); // On lit le caractère

                 if(caractereActuel != '\n')
                 {
                    labyrinthe[i][j] = caractereActuel;
                    j++;
                 }
                 else
                 {
                    i++;
                    j=0;
                 }

             } while (caractereActuel != EOF);
        }//------------------------------------------------------------------------------------------------------------
     fclose(fichier);
}
